package bus

import (
	"github.com/jakebailey/irc"
	"github.com/olebedev/emitter"
)

const (
	all           = "*"
	ircRoot       = "irc/"
	ircIncoming   = ircRoot + "incoming"
	ircOutgoing   = ircRoot + "outgoing"
	ircChannel    = ircRoot + "channel/"
	ircJoin       = ircRoot + "join"
	ircLeave      = ircRoot + "leave"
	ircDisconnect = ircRoot + "disconnect"
	ircOther      = ircRoot + "other"
	whisperRoot   = "whisper/"
	whisperIn     = whisperRoot + "incoming"
	whisperOut    = whisperRoot + "outgoing"
)

// Bus is a bus used by the bot, supporting IRC and string messages.-
type Bus struct {
	e emitter.Emitter
}

// Off closes all event channels.
func (b *Bus) Off() {
	b.OffIncoming()
	b.OffOutgoing()
	b.OffChannel(all)
	b.OffJoin()
	b.OffLeave()
	b.OffDisconnect()
	b.OffOther()
	b.OffWhisperIn()
	b.OffWhisperOut()
}

// Incoming sends an incoming message on the bus.
func (b *Bus) Incoming(m *irc.Message) {
	<-b.e.Emit(ircIncoming, m)
}

// OnIncoming returns a channel of incoming messages.
func (b *Bus) OnIncoming() <-chan *irc.Message {
	return b.onMessage(ircIncoming, false)
}

// OnceIncoming returns a channel of incoming messages that will be closed after
// a single message.
func (b *Bus) OnceIncoming() <-chan *irc.Message {
	return b.onMessage(ircIncoming, true)
}

// OffIncoming closes all of the incoming channels.
func (b *Bus) OffIncoming() {
	b.e.Off(ircIncoming)
}

// Outgoing sends an outgoing message on the bus.
func (b *Bus) Outgoing(m *irc.Message) {
	<-b.e.Emit(ircOutgoing, m)
}

// OnOutgoing returns a channel of outgoing messages.
func (b *Bus) OnOutgoing() <-chan *irc.Message {
	return b.onMessage(ircOutgoing, false)
}

// OnceOutgoing returns a channel of outgoing messages that will be closed after
// a single message.
func (b *Bus) OnceOutgoing() <-chan *irc.Message {
	return b.onMessage(ircOutgoing, true)
}

// OffOutgoing closes all of the outgoing channels.
func (b *Bus) OffOutgoing() {
	b.e.Off(ircOutgoing)
}

// Channel sends a channel message.
func (b *Bus) Channel(c string, m *irc.Message) {
	<-b.e.Emit(ircChannel+c, m)
}

// OnChannel returns a channel of channel messages.
func (b *Bus) OnChannel(c string) <-chan *irc.Message {
	return b.onMessage(ircChannel+c, false)
}

// OnceChannel returns a channel of channel messages that will be closed after
// a single message.
func (b *Bus) OnceChannel(c string) <-chan *irc.Message {
	return b.onMessage(ircChannel+c, true)
}

// OffChannel closes all of the channel message channels.
func (b *Bus) OffChannel(c string) {
	b.e.Off(ircChannel + c)
}

// Join sends a join message on the bus.
func (b *Bus) Join(c string) {
	<-b.e.Emit(ircJoin, c)
}

// OnJoin returns a channel of join messages.
func (b *Bus) OnJoin() <-chan string {
	return b.onString(ircJoin, false)
}

// OnceJoin returns a channel of join messages that will be closed after a
// single message.
func (b *Bus) OnceJoin() <-chan string {
	return b.onString(ircJoin, true)
}

// OffJoin closes all of the join channels.
func (b *Bus) OffJoin() {
	b.e.Off(ircJoin)
}

// Leave sends a leave message on the bus.
func (b *Bus) Leave(c string) {
	<-b.e.Emit(ircLeave, c)
}

// OnLeave returns a channel of leave messages.
func (b *Bus) OnLeave() <-chan string {
	return b.onString(ircLeave, false)
}

// OnceLeave returns a channel of leave messages that will be closed after a
// single message.
func (b *Bus) OnceLeave() <-chan string {
	return b.onString(ircLeave, true)
}

// OffLeave closes all of the leave channels.
func (b *Bus) OffLeave() {
	b.e.Off(ircLeave)
}

// Disconnect sends an disconnect message on the bus.
func (b *Bus) Disconnect() {
	<-b.e.Emit(ircDisconnect, struct{}{})
}

// OnDisconnect returns a channel of disconnect messages.
func (b *Bus) OnDisconnect() <-chan struct{} {
	return b.onEmpty(ircDisconnect, false)
}

// OnceDisconnect returns a channel of disconnect messages that will be closed
// after a single message.
func (b *Bus) OnceDisconnect() <-chan struct{} {
	return b.onEmpty(ircDisconnect, true)
}

// OffDisconnect closes all of the disconnect channels.
func (b *Bus) OffDisconnect() {
	b.e.Off(ircDisconnect)
}

// Other sends an other message on the bus.
func (b *Bus) Other(m *irc.Message) {
	<-b.e.Emit(ircOther, m)
}

// OnOther returns a channel of other messages.
func (b *Bus) OnOther() <-chan *irc.Message {
	return b.onMessage(ircOther, false)
}

// OnceOther returns a channel of other messages that will be closed after a
// single message.
func (b *Bus) OnceOther() <-chan *irc.Message {
	return b.onMessage(ircOther, true)
}

// OffOther closes all of the incoming channels.
func (b *Bus) OffOther() {
	b.e.Off(ircOther)
}

// WhisperIn sends an incoming whisper on the bus.
func (b *Bus) WhisperIn(m *irc.Message) {
	<-b.e.Emit(whisperIn, m)
}

// OnWhisperIn returns a channel of incoming whispers.
func (b *Bus) OnWhisperIn() <-chan *irc.Message {
	return b.onMessage(whisperIn, false)
}

// OnceWhisperIn returns a channel of incoming whispers that will be closed
// after a single message.
func (b *Bus) OnceWhisperIn() <-chan *irc.Message {
	return b.onMessage(whisperIn, true)
}

// OffWhisperIn closes all of the incoming whisper channels.
func (b *Bus) OffWhisperIn() {
	b.e.Off(whisperIn)
}

// WhisperOut sends an outgoing whisper on the bus.
func (b *Bus) WhisperOut(m *irc.Message) {
	<-b.e.Emit(whisperOut, m)
}

// OnWhisperOut returns a channel of outgoing whispers.
func (b *Bus) OnWhisperOut() <-chan *irc.Message {
	return b.onMessage(whisperOut, false)
}

// OnceWhisperOut returns a channel of outgoing whispers that will be closed
// after a single message.
func (b *Bus) OnceWhisperOut() <-chan *irc.Message {
	return b.onMessage(whisperOut, true)
}

// OffWhisperOut closes all of the outgoing whisper channels.
func (b *Bus) OffWhisperOut() {
	b.e.Off(whisperOut)
}
