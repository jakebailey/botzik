package bus

import (
	"github.com/jakebailey/irc"
	"github.com/olebedev/emitter"
)

func (b *Bus) onOrOnce(pattern string, once bool) <-chan emitter.Event {
	if once {
		return b.e.Once(pattern)
	}
	return b.e.On(pattern)
}

func (b *Bus) onMessage(pattern string, once bool) <-chan *irc.Message {
	ch := make(chan *irc.Message)
	go func() {
		for e := range b.onOrOnce(pattern, once) {
			for _, i := range e.Args {
				ch <- i.(*irc.Message)
			}
		}
		close(ch)
	}()
	return ch
}

func (b *Bus) onString(pattern string, once bool) <-chan string {
	ch := make(chan string)
	go func() {
		for e := range b.onOrOnce(pattern, once) {
			for _, i := range e.Args {
				ch <- i.(string)
			}
		}
		close(ch)
	}()
	return ch
}

func (b *Bus) onEmpty(pattern string, once bool) <-chan struct{} {
	ch := make(chan struct{})
	go func() {
		for e := range b.onOrOnce(pattern, once) {
			for _, i := range e.Args {
				ch <- i.(struct{})
			}
		}
		close(ch)
	}()
	return ch
}
