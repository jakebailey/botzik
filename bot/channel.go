package bot

import (
	"log"

	"bitbucket.org/jakebailey/botzik/command"
)

func (b *Bot) channel(c string) {
	b.wg.Add(1)
	defer b.wg.Done()

	for m := range b.b.OnChannel(c) {
		func() {
			defer func() {
				if r := recover(); r != nil {
					log.Println(r)
				}
			}()
			if err := command.Run(m, b.s); err != nil {
				log.Println(c+":", err)
			}
		}()
	}
}
