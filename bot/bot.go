package bot

import (
	"sync"

	"bitbucket.org/jakebailey/botzik/birc"
	"bitbucket.org/jakebailey/botzik/bus"
	"bitbucket.org/jakebailey/botzik/db"
	"bitbucket.org/jakebailey/botzik/services"
	"github.com/xyproto/pinterface"
)

// Options describe the configuration of the bot.
type Options struct {
	Addr       string
	Nick       string
	Pass       string
	DBType     string
	DBOptions  string
	Superusers []string
	Initial    []string
}

// Bot is an IRC bot.
type Bot struct {
	opts      Options
	b         *bus.Bus
	conn      birc.Conn
	db        pinterface.ICreator
	dbClose   func()
	wg        *sync.WaitGroup
	stopping  bool
	joinedSet map[string]bool
	joinedMu  sync.RWMutex
	s         services.Services
	whisper   birc.Whisper
}

// New creates a new bot with the given options
func New(opts Options) *Bot {
	b := &bus.Bus{}
	wg := &sync.WaitGroup{}

	supers := make(map[string]bool)
	for _, u := range opts.Superusers {
		supers[u] = true
	}

	return &Bot{
		opts: opts,
		b:    b,
		wg:   wg,
		s: services.Services{
			Bus: b,
			WG:  wg,
			IsSuper: func(name string) (bool, error) {
				return supers[name], nil
			},
		},
	}
}

// Start starts the bot.
func (b *Bot) Start() (err error) {
	defer func() {
		if err != nil {
			if b.dbClose != nil {
				b.dbClose()
			}
			b.b.Off()
		}
	}()

	b.db, b.dbClose, err = db.New(b.opts.DBType, b.opts.DBOptions)
	if err != nil {
		return err
	}
	b.s.DB = b.db

	return b.startIRC()
}

// Stop stops the bot.
func (b *Bot) Stop() {
	b.stopping = true

	b.b.Off()
	b.conn.Disconnect()
	b.whisper.Disconnect()
	b.wg.Wait()
	b.dbClose()
}
