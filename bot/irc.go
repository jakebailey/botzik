package bot

import (
	"fmt"
	"log"

	"github.com/jakebailey/irc"

	"bitbucket.org/jakebailey/botzik/birc"
)

func (b *Bot) startIRC() error {
	b.conn = birc.NewPool(b.opts.Addr, b.opts.Nick, b.opts.Pass)
	if err := b.conn.Connect(); err != nil {
		return err
	}

	b.whisper = birc.NewWhisper(b.opts.Addr, b.opts.Nick, b.opts.Pass)
	if err := b.whisper.Connect(); err != nil {
		return err
	}

	b.joinedSet = make(map[string]bool)
	for _, c := range b.opts.Initial {
		b.conn.Join(c)
		b.join(c)
	}

	go b.decoder()
	go b.encoder()
	go b.joiner()
	go b.leaver()
	go b.whisperReciever()
	go b.whisperSender()

	return nil
}

func (b *Bot) decoder() {
	b.wg.Add(1)
	defer b.wg.Done()

	for m := range b.conn.Incoming() {
		if birc.IsPrivMSG(m) {
			if m.Prefix.User == b.opts.Nick {
				continue
			}

			log.Println("->", "["+m.Params[0]+"]", m.Prefix.User+":", m.Trailing)
			b.b.Channel(m.Params[0], m)
			continue
		}

		log.Println("->", m)
		b.b.Other(m)
	}
}

func (b *Bot) encoder() {
	b.wg.Add(1)
	defer b.wg.Done()

	for m := range b.b.OnOutgoing() {
		log.Println("<-", m)
		b.conn.Send(m)
	}
}

func (b *Bot) joiner() {
	b.wg.Add(1)
	defer b.wg.Done()

	for c := range b.b.OnJoin() {
		if b.joined(c) {
			log.Println("already joined", c)
			continue
		}

		log.Println("joining", c)

		b.conn.Join(c)
		b.join(c)
	}
}

func (b *Bot) leaver() {
	b.wg.Add(1)
	defer b.wg.Done()

	for c := range b.b.OnLeave() {

		if c == "#"+b.opts.Nick {
			continue
		}

		if !b.joined(c) {
			log.Println("already left", c)
			continue
		}

		log.Println("leaving", c)
		b.conn.Part(c)
		b.leave(c)
	}
}

func (b *Bot) join(c string) {
	b.joinedMu.Lock()
	defer b.joinedMu.Unlock()
	b.joinedSet[c] = true

	go b.channel(c)
}

func (b *Bot) joined(c string) bool {
	b.joinedMu.RLock()
	defer b.joinedMu.RUnlock()
	return b.joinedSet[c]
}

func (b *Bot) leave(c string) {
	b.joinedMu.Lock()
	defer b.joinedMu.Unlock()
	delete(b.joinedSet, c)

	b.b.OffChannel(c)
}

func (b *Bot) whisperReciever() {
	b.wg.Add(1)
	defer b.wg.Done()

	for m := range b.whisper.Incoming() {
		if m.Command != "WHISPER" {
			b.b.Other(m)
			continue
		}

		log.Println("WHISPER", m.Prefix.User+":", m.Trailing)
		b.b.WhisperIn(m)
		b.b.WhisperOut(&irc.Message{
			Command:  irc.PRIVMSG,
			Params:   []string{"#jtv"},
			Trailing: fmt.Sprintf("/w %v %v", m.Prefix.User, m.Trailing),
		})
	}
}

func (b *Bot) whisperSender() {
	b.wg.Add(1)
	defer b.wg.Done()

	for m := range b.b.OnWhisperOut() {
		if !b.whisper.Send(m) {
			log.Printf("failed to send whisper: %v", m)
		}
	}
}
