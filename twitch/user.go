package twitch

import (
	"bitbucket.org/jakebailey/botzik/perm"
	"github.com/jakebailey/irc"
)

// User describes a Twitch user in the context of a PRIVMSG.
type User struct {
	Name        string
	Level       perm.Level
	Turbo       bool
	Regular     bool
	Sub         bool
	Mod         bool
	Broadcaster bool
	Superuser   bool
	Color       string
}

// GetUser builds a User from a PRIVMSG.
func GetUser(m *irc.Message, isSuper func(string) (bool, error), isRegular func(string) (bool, error)) (User, error) {
	var err error
	u := User{Level: perm.Normal}

	var ok bool
	u.Name, ok = m.GetTag("display-name")
	if !ok || u.Name == "" {
		if m.Prefix != nil {
			u.Name = m.Prefix.User
		} else {
			u.Name = "*UNKNOWN*"
		}
	}

	u.Turbo = tagBool(m, "turbo")

	u.Regular, err = isRegular(m.Prefix.User)
	if err != nil {
		return User{}, err
	}
	if u.Regular {
		u.Level = perm.Regular
	}

	u.Sub = tagBool(m, "subscriber")
	if u.Sub {
		u.Level = perm.Sub
	}

	u.Mod = tagBool(m, "mod")
	if u.Mod {
		u.Level = perm.Mod
	}

	if m.Params[0][1:] == m.Prefix.User {
		u.Broadcaster = true
		u.Level = perm.Broadcaster
	}

	u.Superuser, err = isSuper(m.Prefix.User)
	if err != nil {
		return User{}, err
	}
	if u.Superuser {
		u.Level = perm.Superuser
	}

	u.Color, ok = m.GetTag("color")
	if !ok || u.Color == "" {
		u.Color = "#FFFFFF"
	}

	return u, nil
}

func tagBool(m *irc.Message, tag string) bool {
	s, ok := m.GetTag(tag)
	return ok && s == "1"
}
