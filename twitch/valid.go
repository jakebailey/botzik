package twitch

import "regexp"

// Validity regexps
var (
	validUsername = regexp.MustCompile(`^[[:alnum:]]\w{3,24}$`)
	validChannel  = regexp.MustCompile(`^#[[:alnum:]]\w{3,24}$`)
)

// ValidUsername returns true if a Twitch username is valid.
func ValidUsername(s string) bool {
	return validUsername.MatchString(s)
}

// ValidChannel returns true if a Twtich channel is valid.
func ValidChannel(s string) bool {
	return validChannel.MatchString(s)
}
