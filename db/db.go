package db

import (
	"errors"

	"github.com/xyproto/pinterface"
	"github.com/xyproto/simplebolt"
	"github.com/xyproto/simpleredis"
)

// ErrInvalidType is returned when an invalid database type is provided to New.
var ErrInvalidType = errors.New("invalid database type")

// Valid database types.
const (
	Bolt  = "bolt"
	Redis = "redis"
)

// New creates a new ICreator from the specified database. Be sure to call
// the close function to close the database.
func New(ty string, options string) (db pinterface.ICreator, close func(), err error) {
	switch ty {
	case Bolt:
		return bolt(options)
	case Redis:
		return redis(options)
	default:
		return nil, nil, ErrInvalidType
	}
}

func bolt(options string) (db pinterface.ICreator, close func(), err error) {
	b, err := simplebolt.New(options)
	if err != nil {
		return nil, nil, err
	}
	return simplebolt.NewCreator(b), b.Close, nil
}

func redis(options string) (db pinterface.ICreator, close func(), err error) {
	pool := simpleredis.NewConnectionPoolHost(options)
	if err := pool.Ping(); err != nil {
		return nil, nil, err
	}
	return simpleredis.NewCreator(pool, 0), pool.Close, nil
}
