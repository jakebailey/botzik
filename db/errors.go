package db

import (
	"errors"

	"github.com/xyproto/simplebolt"
)

// errors
var (
	ErrKeyNotFound  = errors.New("Key not found")
	ErrDoesNotExist = errors.New("Does not exist")
	ErrFoundIt      = errors.New("Found it")
	ErrExistsInSet  = errors.New("Element already exists in set")
)

// Error converts an error to one in this package, or returns the original error
// if the error cannot be converted.
func Error(err error) error {
	switch err {
	case simplebolt.ErrKeyNotFound:
		return ErrKeyNotFound
	case simplebolt.ErrDoesNotExist:
		return ErrDoesNotExist
	case simplebolt.ErrFoundIt:
		return ErrFoundIt
	case simplebolt.ErrExistsInSet:
		return ErrExistsInSet
	}
	return err
}
