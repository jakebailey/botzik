# botzik

botzik is a Twitch IRC bot. I created this during the summer of 2016 for fun.
Below is an overview of my opinion 2+ years on.


## The good

- The bot's IRC backend is great. It uses my (older) IRC library, and adds a
layer on top of it to add connection management. This means that the bot
should be able to join any number of channels and send any number of messages
at any rate and not be globalled. It will dynamically create and destroy
connections on the fly based on load.
- The way builtin commands are written is easy to manage. There's a global
registry that's called from different files, rather than some big list of them.
- Builtin commands can have documentation.
- Commands can traverse channels.
- You can send commands over whispers (which is probably best used for
administration).
- You can alias builtins (which is good if there's a builtin with a name a user
wants to use). To access a builtin even if it's aliased, you can do it by
doubling up the command prefix (i.e. `!!list quotes get 42`).


## The mediocre

- User-visible "commands" are mainly alias powered, i.e. to make a quote
command, you'd alias `list quote` to `quote`, and then it'd just work. This is
handy, but you'd end up with a web of aliases (and there's no way to execute
multiple commands).
- Permissions could be a bit more granular. There are actual libraries for
access controls, but I didn't use them.
- Command prefixes (`$+!`, etc) aren't checked, so you could theoretically
make an alias on the prefix and lose double-prefix overrides.
- Aliases are just prefix replacements, not a nice variable replacement
(it'd be nice to turn into sh-style replacements).
- The override being a double prefix before the command sort of breaks
the `++b` jokes from mods. :P


## The bad

- The IRC lib used is the older one, whose repo I moved. This repo still works
thanks to vendoring, but would need to be migrated.
- Although the core IRC stuff is okay, the way the messages make it out to be
handled is (in hindsight) gross, making use of a pub-sub system with a lot of
repetition. It may have been better to write this more like net/http, or just
a plain `func(m *irc.Message) error`.
- The DB uses a generic KV-store interface library, which has been abandoned by
its owner.


## The missing

- No actual channel moderation features.
- No resub / Twitch notification stuff.
- To do an upgrade, you kill the bot and restart it. A better method would
involve splitting apart the IRC reading from the actual handling code so that
the IRC connections remain open (and pending) while the handler is locked and
being swapped.
