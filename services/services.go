package services

import (
	"sync"

	"bitbucket.org/jakebailey/botzik/bus"
	"github.com/xyproto/pinterface"
)

// Services holds commonly used items in a bot.
type Services struct {
	Bus     *bus.Bus
	WG      *sync.WaitGroup
	DB      pinterface.ICreator
	IsSuper func(string) (bool, error)
}
