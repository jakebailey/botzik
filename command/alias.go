package command

import (
	"fmt"

	"bitbucket.org/jakebailey/botzik/perm"
)

func init() {
	Register(Command{
		Name:        "alias",
		Description: "Manages aliases.",
		Subcommands: []string{Get, Set, Del},
		Level:       perm.Superuser,
		Fn:          aliasCommand,
	})
}

func aliasCommand(r *Runner) error {
	if len(r.Arg) == 0 {
		return ErrNotEnoughArguments
	}

	aliaser, err := r.Channel.Aliaser()
	if err != nil {
		return err
	}

	name, cmd, cmdArg := r.SplitForCommand()

	switch cmd {
	case Get, "":
		v, err := aliaser.Get(name)
		if err != nil {
			return err
		}
		r.Send(v)
	case Set:
		if len(cmdArg) == 0 {
			return ErrNotEnoughArguments
		}

		if err := aliaser.Set(name, cmdArg); err != nil {
			return err
		}

		var warning string
		if _, ok := registry[name]; ok {
			warning = fmt.Sprintf(" (warning: %v is a builtin)", name)
		}
		r.Sendf("%v set to `%v`%v", name, cmdArg, warning)
	case Del:
		if err := aliaser.Del(name); err != nil {
			return err
		}
		r.Sendf("%v removed", name)
	}

	return nil
}
