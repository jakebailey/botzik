package command

import (
	"fmt"
	"log"
	"os"
	"strings"
	"unicode"

	"bitbucket.org/jakebailey/botzik/channel"
	"bitbucket.org/jakebailey/botzik/db"
	"bitbucket.org/jakebailey/botzik/perm"
	"bitbucket.org/jakebailey/botzik/services"
	"bitbucket.org/jakebailey/botzik/twitch"
	"github.com/jakebailey/irc"
)

// Runner contains the information to run a command.
type Runner struct {
	Message      *irc.Message
	Services     services.Services
	Channel      channel.Channel
	User         twitch.User
	Arg          string
	Prefix       string
	Quiet        bool
	IgnorePrefix bool
	IgnoreRate   bool
	Redirect     string
}

// Run runs a command based on a message.
func Run(m *irc.Message, s services.Services) error {
	trailing := strings.Trim(m.Trailing, "\x01")
	if len(trailing) != len(m.Trailing) {
		trailing = strings.TrimPrefix(trailing, "ACTION ")
	}
	m.Trailing = strings.TrimSpace(trailing)

	r := Runner{
		Message:  m,
		Services: s,
	}
	return r.Run()
}

// Run runs a command with a runner.
func (r *Runner) Run() error {
	var err error
	r.Channel = channel.New(r.Message.Params[0], r.Services.DB)
	r.Prefix, err = r.Channel.Prefix()
	if err != nil {
		return err
	}
	r.User, err = twitch.GetUser(r.Message, r.Services.IsSuper, r.Channel.IsRegular)
	if err != nil {
		return err
	}

	input := r.Message.Trailing
	if !r.IgnorePrefix {
		if !strings.HasPrefix(input, r.Prefix) {
			if perm.Allowed(r.User.Level, perm.Regular) {
				ok, rErr := r.CheckRate()
				if rErr != nil {
					return rErr
				}
				if ok {
					r.IgnoreRate = true
					updateMessageTime(r.Channel.Name)
					return r.CheckMatches()
				}
			}
			return nil
		}
		input = strings.TrimPrefix(input, r.Prefix)
	}

	skipAlias := false
	if strings.HasPrefix(input, r.Prefix) {
		skipAlias = true
		input = strings.TrimPrefix(input, r.Prefix)
	}

	input = strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return ' '
		}
		return r
	}, input)

	aliaser, err := r.Channel.Aliaser()
	if err != nil {
		return err
	}

	args := strings.SplitN(input, " ", 2)
	var cmd string
	switch len(args) {
	default:
		fallthrough
	case 2:
		r.Arg = args[1]
		fallthrough
	case 1:
		cmd = args[0]
	case 0:
		return nil
	}

	if !skipAlias {
		if newCmd, aErr := aliaser.Get(cmd); aErr == nil || db.Error(aErr) == db.ErrFoundIt {
			newCmd = os.Expand(newCmd, func(v string) string {
				if v == "ARGS" {
					return r.Arg
				}
				return ""
			})
			log.Println(r.Channel.Name+":", "expanding", input, "to", newCmd)
			r.Message.Trailing = newCmd
			r.IgnorePrefix = true

			ok, rErr := r.CheckRate()
			if rErr != nil {
				return rErr
			}
			if !ok {
				return nil
			}

			r.IgnoreRate = true
			updateMessageTime(r.Channel.Name)
			return r.Run()
		}
	}

	if command, ok := registry[cmd]; ok {
		if !perm.Allowed(r.User.Level, command.Level) {
			return nil
		}

		log.Println(r.Channel.Name+":", "running command", command.Name)
		return command.Fn(r)
	}

	ok, rErr := r.CheckRate()
	if rErr != nil {
		return rErr
	}
	if !ok {
		return nil
	}

	if perm.Allowed(r.User.Level, perm.Regular) {
		r.IgnoreRate = true
		updateMessageTime(r.Channel.Name)
		return r.CheckMatches()
	}
	return nil
}

// CheckMatches checks the current message for matches, and sends a response
// if found.
func (r *Runner) CheckMatches() error {
	matcher, err := r.Channel.Matcher()
	if err != nil {
		return err
	}
	resp, err := matcher.Match(r.Message.Trailing)
	if err != nil {
		return err
	}
	if resp != "" {
		r.Send(resp)
	}
	return nil
}

// Send sends a  response.
func (r *Runner) Send(message string) {
	if r.Quiet {
		log.Println("<-", Response(r.Channel.Name, message))
	} else {
		channel := r.Channel.Name
		if r.Redirect != "" {
			channel = r.Redirect
		}
		r.Services.Bus.Outgoing(Response(channel, message))
	}
}

// Sendf sends a formatted response.
func (r *Runner) Sendf(format string, args ...interface{}) {
	r.Send(fmt.Sprintf(format, args...))
}

// SplitForCommand splits a Runner's args into a name, a command, and args.
func (r *Runner) SplitForCommand() (name string, cmd string, cmdArg string) {
	a := strings.Split(r.Arg, " ")
	switch len(a) {
	default:
		fallthrough
	case 3:
		cmdArg = strings.Join(a[2:], " ")
		fallthrough
	case 2:
		cmd = a[1]
		fallthrough
	case 1:
		name = a[0]
	case 0:
	}
	return name, cmd, cmdArg
}

// CheckRate checks if this runner can run a command.
func (r *Runner) CheckRate() (bool, error) {
	if r.IgnoreRate {
		return true, nil
	}

	if perm.Allowed(r.User.Level, perm.Mod) {
		return true, nil
	}

	dur, ok := getMessageDur(r.Channel.Name)
	if ok {
		return true, nil
	}

	rate, err := r.Channel.Rate()
	if err != nil {
		return false, err
	}

	if rate > dur {
		return false, nil
	}
	return true, nil
}
