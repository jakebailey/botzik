package command

import (
	"errors"
	"log"

	"bitbucket.org/jakebailey/botzik/perm"
)

// Command errors
var (
	ErrNotAllowed         = errors.New("not allowed")
	ErrNotEnoughArguments = errors.New("not enough arguments")
	ErrTODO               = errors.New("TODO")
)

// Command represents a bot command.
type Command struct {
	Name        string
	Description string
	Subcommands []string
	Level       perm.Level
	Fn          func(r *Runner) error
}

var registry = make(map[string]Command)

// Register registers a command.
func Register(cs ...Command) {
	for _, c := range cs {
		if c.Name == "" {
			log.Panicln("command must have name")
		}
		if c.Level == perm.Unknown {
			log.Panicf("command %v must have default level set", c.Name)
		}

		if _, ok := registry[c.Name]; ok {
			log.Panicln("duplicate command registered:", c.Name)
		}

		log.Printf("registering command %v", c.Name)
		registry[c.Name] = c
	}
}
