package command

import "github.com/jakebailey/irc"

// Response creates a simple PRIVMSG to a channel with a message.
func Response(channel, message string) *irc.Message {
	return &irc.Message{
		Command:  irc.PRIVMSG,
		Params:   []string{channel},
		Trailing: message,
	}
}
