package command

import (
	"math/rand"
	"strconv"
	"strings"

	"bitbucket.org/jakebailey/botzik/perm"
)

func init() {
	Register(Command{
		Name:        "list",
		Description: "Accesses the list specified in the first argument.",
		Subcommands: []string{Len, Get, Set, App, Add, Del, Rand, Random, Rem, Qrand},
		Level:       perm.Superuser,
		Fn:          listCommand,
	})
}

func listCommand(r *Runner) error {
	if len(r.Arg) == 0 {
		return ErrNotEnoughArguments
	}

	name, cmd, cmdArg := r.SplitForCommand()

	list, err := r.Channel.Array(name)
	if err != nil {
		return err
	}

	switch cmd {
	case Len:
		length, err := list.Length()
		if err != nil {
			return ErrTODO
		}
		r.Send(strconv.Itoa(length))
	case Get:
		if len(cmdArg) == 0 {
			return ErrNotEnoughArguments
		}
		i, err := strconv.Atoi(cmdArg)
		if err != nil {
			return err
		}
		s, err := list.Get(i)
		if err != nil {
			return err
		}
		r.Sendf(`%v: "%v"`, i, s)
	case App, Add:
		if len(cmdArg) == 0 {
			return ErrNotEnoughArguments
		}
		i, err := list.Append(cmdArg)
		if err != nil {
			return err
		}
		r.Sendf(`%v[%v] = "%v"`, name, i, cmdArg)
	case Set:
		split := strings.SplitN(cmdArg, " ", 2)
		if len(split) == 0 {
			return ErrNotEnoughArguments
		}

		i, err := strconv.Atoi(split[0])
		if err != nil {
			return err
		}
		v := split[1]
		if err := list.Set(i, v); err != nil {
			return err
		}
		r.Sendf(`%v[%v] = "%v"`, name, i, v)
	case Del:
		if len(cmdArg) == 0 {
			return ErrNotEnoughArguments
		}
		i, err := strconv.Atoi(cmdArg)
		if err != nil {
			return err
		}
		if err := list.Delete(i); err != nil {
			return err
		}
		r.Sendf("deleted %v[%v]", name, i)
	case Rem:
		if err := list.Remove(); err != nil {
			return err
		}
		r.Sendf("removed %v", name)
	case Rand, "", Qrand:
		length, err := list.Length()
		if err != nil {
			return err
		}
		if length == 0 {
			return nil
		}
		i := rand.Intn(length)
		s, err := list.Get(i)
		if err != nil {
			return err
		}
		if cmd == Qrand {
			r.Send(s)
		} else {
			r.Sendf(`%v: "%v"`, i, s)
		}
	}

	return nil
}
