package command

import "bitbucket.org/jakebailey/botzik/perm"

func init() {
	Register(Command{
		Name:        "prefix",
		Description: "Sets a channel's prefix",
		Level:       perm.Superuser,
		Fn: func(r *Runner) error {
			if len(r.Arg) == 0 {
				return ErrNotEnoughArguments
			}

			if err := r.Channel.SetPrefix(r.Arg); err != nil {
				return err
			}

			r.Sendf("prefix set to `%v`", r.Arg)
			return nil
		},
	})
}
