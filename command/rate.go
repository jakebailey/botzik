package command

import (
	"sync"
	"time"

	"bitbucket.org/jakebailey/botzik/perm"
)

var (
	lastMessageTimes = make(map[string]time.Time)
	lastMessageMu    sync.Mutex
)

func init() {
	Register(Command{
		Name:        "rate",
		Description: "Sets the rate at which non-moderater and above messages will be checked.",
		Level:       perm.Superuser,
		Fn: func(r *Runner) error {
			if r.Arg == "" {
				rate, err := r.Channel.Rate()
				if err != nil {
					return err
				}
				r.Sendf("current rate is: %v", rate)
				return nil
			}

			rate, err := time.ParseDuration(r.Arg)
			if err != nil {
				r.Sendf("invalid rate: %v", err.Error())
				return err
			}

			if err := r.Channel.SetRate(rate); err != nil {
				return err
			}

			r.Sendf("setting rate to: %v", rate.String())
			return nil
		},
	})
}

func getMessageDur(channel string) (t time.Duration, b bool) {
	lastMessageMu.Lock()
	defer lastMessageMu.Unlock()

	last, ok := lastMessageTimes[channel]
	if !ok {
		lastMessageTimes[channel] = time.Now()
		return time.Duration(0), true
	}
	return time.Now().Sub(last), false
}

func updateMessageTime(channel string) {
	lastMessageMu.Lock()
	defer lastMessageMu.Unlock()
	lastMessageTimes[channel] = time.Now()
}
