package command

import "bitbucket.org/jakebailey/botzik/perm"

func init() {
	Register(Command{
		Name:  "bones",
		Level: perm.Superuser,
		Fn: func(r *Runner) error {
			r.Send(`L̶͘O͜N̕G҉̷ ҉Ĺ͟IV̨͏E҉̴̷ ̷T̶̕H̵̡̧Ȩ̛ ͟Ç̛ULT͘ ̧̡̀O̸̸F̀ ̵̧BÒ̶̴N̸Ę͞Ś̀͞`)
			return nil
		},
	}, Command{
		Name:  "clark",
		Level: perm.Superuser,
		Fn: func(r *Runner) error {
			r.Send("CHEESE?")
			return nil
		},
	})
}
