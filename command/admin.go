package command

import (
	"strconv"
	"strings"

	"github.com/jakebailey/irc"

	"bitbucket.org/jakebailey/botzik/perm"
	"bitbucket.org/jakebailey/botzik/twitch"
)

func init() {
	Register(Command{
		Name:        "say",
		Description: "Says the specified message.",
		Level:       perm.Superuser,
		Fn: func(r *Runner) error {
			r.Send(r.Arg)
			return nil
		},
	}, Command{
		Name:        "join",
		Description: "Joins the specified channels.",
		Level:       perm.Superuser,
		Fn:          func(r *Runner) error { return joinLeave(r, true) },
	}, Command{
		Name:        "leave",
		Description: "Leaves the specified channels.",
		Level:       perm.Superuser,
		Fn:          func(r *Runner) error { return joinLeave(r, false) },
	}, Command{
		Name:        "quiet",
		Description: "Runs the specified command quietly.",
		Level:       perm.Superuser,
		Fn: func(r *Runner) error {
			r.Quiet = true
			r.IgnorePrefix = true
			r.Message.Trailing = r.Arg
			return r.Run()
		},
	}, Command{
		Name:        "#",
		Description: "Runs the specified command in another channel.",
		Level:       perm.Superuser,
		Fn:          func(r *Runner) error { return crossChannel(r, false) },
	}, Command{
		Name:        "##",
		Description: "Runs the specified command in another channel, redirecting output to this channel.",
		Level:       perm.Superuser,
		Fn:          func(r *Runner) error { return crossChannel(r, true) },
	}, Command{
		Name:  "quit",
		Level: perm.Superuser,
		Fn: func(r *Runner) error {
			r.Services.Bus.Outgoing(&irc.Message{Command: irc.QUIT})
			return nil
		},
	}, Command{
		Name:        "spam",
		Description: "Spams a message.",
		Level:       perm.Superuser,
		Fn: func(r *Runner) error {
			split := strings.SplitN(r.Arg, " ", 2)
			if len(split) != 2 || split[0] == "" {
				return ErrNotEnoughArguments
			}
			num, err := strconv.Atoi(split[0])
			if err != nil {
				return err
			}

			for i := 0; i < num; i++ {
				r.Send(split[1])
			}

			return nil
		},
	})
}

func joinLeave(r *Runner, join bool) error {
	msg := "join"
	if !join {
		msg = "leave"
	}

	var cs []string
	for _, c := range strings.Fields(r.Arg) {
		if twitch.ValidUsername(c) {
			cs = append(cs, c)
			if join {
				r.Services.Bus.Join(c)
			} else {
				r.Services.Bus.Leave(c)
			}
		}
	}
	if len(cs) == 0 {
		r.Send("no valid channels in command.")
	} else {
		r.Sendf("attempting to %v: %v", msg, strings.Join(cs, ", "))
	}
	return nil
}

func crossChannel(r *Runner, redirect bool) error {
	split := strings.SplitN(r.Arg, " ", 2)
	if len(split) != 2 {
		return nil
	}
	if !twitch.ValidUsername(split[0]) {
		return nil
	}
	r.Message.Params[0] = "#" + split[0]
	if redirect && r.Redirect == "" {
		r.Redirect = r.Channel.Name
	}
	r.Message.Trailing = split[1]
	r.IgnorePrefix = true
	return r.Run()
}
