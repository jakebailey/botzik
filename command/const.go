package command

// Common subcommands.
const (
	Add    = "add"
	App    = "app"
	Del    = "del"
	Get    = "get"
	Inc    = "inc"
	Len    = "len"
	Rand   = "rand"
	Random = "random"
	Rem    = "rem"
	Set    = "set"
	Qrand  = "qrand"
)
