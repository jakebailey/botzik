package command

import (
	"strings"

	"bitbucket.org/jakebailey/botzik/perm"
)

func init() {
	Register(Command{
		Name:        "regular",
		Description: "Manages regulars",
		Level:       perm.Superuser,
		Subcommands: []string{Add, Rem},
		Fn: func(r *Runner) error {
			split := strings.SplitN(r.Arg, " ", 2)
			if len(split) != 2 || split[0] == "" {
				return ErrNotEnoughArguments
			}

			switch split[0] {
			case Add:
				if err := r.Channel.AddRegular(split[1]); err != nil {
					return err
				}
				r.Sendf("added regular %v", split[1])
			case Rem:
				if err := r.Channel.RemoveRegular(split[1]); err != nil {
					return err
				}
				r.Sendf("removed regular %v", split[1])
			}

			return nil
		},
	})
}
