package command

import (
	"strings"

	"bitbucket.org/jakebailey/botzik/perm"
)

func init() {
	Register(Command{
		Name:        "match",
		Description: "Manages message matchers and responses.",
		Subcommands: []string{Get, Set, Del},
		Level:       perm.Superuser,
		Fn:          matchCommand,
	})
}

func matchCommand(r *Runner) error {
	if len(r.Arg) == 0 {
		return ErrNotEnoughArguments
	}

	matcher, err := r.Channel.Matcher()
	if err != nil {
		return err
	}

	name, cmd, cmdArg := r.SplitForCommand()

	switch cmd {
	case Get, "":
		expr, resp, err := matcher.Get(name)
		if err != nil {
			return err
		}
		r.Sendf("expr: `%v`, resp: `%v`", expr, resp)
	case Set:
		split := strings.SplitN(cmdArg, " ;; ", 2)
		if len(split) != 2 {
			return ErrNotEnoughArguments
		}
		expr := split[0]
		resp := split[1]
		if err := matcher.Set(name, expr, resp); err != nil {
			return err
		}
		r.Sendf("set matcher %v, expr: `%v`, resp: `%v`", name, expr, resp)
	case Del:
		if err := matcher.Delete(name); err != nil {
			return err
		}
		r.Sendf("deleted matcher %v", name)
	}

	return nil
}
