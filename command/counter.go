package command

import (
	"strconv"

	"bitbucket.org/jakebailey/botzik/perm"
)

func init() {
	Register(Command{
		Name:        "counter",
		Description: "Accesses the counter specified in the first argument.",
		Subcommands: []string{Get, Set, Inc, Rem},
		Level:       perm.Superuser,
		Fn:          counterCommand,
	})
}

func counterCommand(r *Runner) error {
	if len(r.Arg) == 0 {
		return ErrNotEnoughArguments
	}

	name, cmd, cmdArg := r.SplitForCommand()

	counter, err := r.Channel.Counter(name)
	if err != nil {
		return err
	}

	switch cmd {
	case Get, "":
		v, err := counter.Get()
		if err != nil {
			return err
		}
		r.Send(strconv.Itoa(v))
	case Set:
		if len(cmdArg) == 0 {
			return ErrNotEnoughArguments
		}
		v, err := strconv.Atoi(cmdArg)
		if err != nil {
			return err
		}
		if err := counter.Set(v); err != nil {
			return err
		}
		r.Sendf("%v set to %v", name, v)
	case Inc:
		i := 1
		if len(cmdArg) == 0 {
			var err error
			i, err = strconv.Atoi(cmdArg)
			if err != nil {
				return err
			}
		}
		v, err := counter.Increment(i)
		if err != nil {
			return err
		}
		r.Sendf("%v incremeneted by %v to %v", name, i, v)
	case Rem:
		if err := counter.Remove(); err != nil {
			return err
		}
		r.Sendf("removed %v", name)
	}

	return nil
}
