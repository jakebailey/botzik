package command

import (
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/jakebailey/botzik/perm"
)

func init() {
	Register(Command{
		Name:        "info",
		Description: "Accesses information about commands.",
		Level:       perm.Superuser,
		Fn: func(r *Runner) error {
			if len(r.Arg) == 0 {
				return nil
			}
			cmd, ok := registry[r.Arg]
			if !ok {
				return nil
			}

			var message string
			if cmd.Description != "" {
				message = " " + cmd.Description
			}
			if len(cmd.Subcommands) > 0 {
				message += " Subcommands: " + strings.Join(cmd.Subcommands, ", ")
			}
			message += fmt.Sprintf(" Level: %v", cmd.Level)

			r.Send(cmd.Name + ":" + message)
			return nil
		},
	}, Command{
		Name:        "builtins",
		Description: "Lists all built-in commands.",
		Level:       perm.Superuser,
		Fn: func(r *Runner) error {
			l := make(sort.StringSlice, len(registry))
			i := 0
			for c := range registry {
				l[i] = c
				i++
			}
			l.Sort()

			r.Send(strings.Join(l, ", "))
			return nil
		},
	})
}
