package channel

import "github.com/xyproto/pinterface"

func (c Channel) hashMap(name string) (pinterface.IHashMap, error) {
	return c.db.NewHashMap(c.id("hashmap", name))
}

// HashMap returns the hash map of the specified name.
func (c Channel) HashMap(name string) (pinterface.IHashMap, error) {
	return c.hashMap(externalPrefix + name)
}
