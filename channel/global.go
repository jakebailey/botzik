package channel

import (
	"log"

	"bitbucket.org/jakebailey/botzik/db"
	"github.com/xyproto/pinterface"
)

// GlobalKV returns the returns the global key value store.
func (c Channel) GlobalKV() (pinterface.IKeyValue, error) {
	return c.kv(globalName)
}

// GlobalKVGetDefault gets a value from the global KV, or sets a default if not
// found.
func (c Channel) GlobalKVGetDefault(key, def string) (string, error) {
	kv, err := c.GlobalKV()
	if err != nil {
		return "", err
	}

	value, err := kv.Get(key)
	err = db.Error(err)
	if err == db.ErrKeyNotFound {
		log.Printf("%v: setting default %v to %v", c.Name, key, def)
		err = kv.Set(key, def)
		if err != nil {
			return "", err
		}
		value = def
	}

	return value, nil
}

// GlobalKVSet sets a value in the global KV.
func (c Channel) GlobalKVSet(key, value string) error {
	kv, err := c.GlobalKV()
	if err != nil {
		return err
	}
	return kv.Set(key, value)
}
