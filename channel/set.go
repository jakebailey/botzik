package channel

import "github.com/xyproto/pinterface"

func (c Channel) set(name string) (pinterface.ISet, error) {
	return c.db.NewSet(c.id("set", name))
}

// Set returns the set of the specified name.
func (c Channel) Set(name string) (pinterface.ISet, error) {
	return c.set(externalPrefix + name)
}
