package channel

import (
	"regexp"
	"sync"
	"time"
)

const (
	regexRate     = 10 * time.Minute
	regexAgeLimit = 30 * time.Minute
)

var (
	regexCache = make(map[string]regexResp)
	regexAge   = make(map[string]time.Time)
	regexMu    sync.RWMutex
)

func init() {
	go func() {
		for range time.Tick(regexRate) {
			regexMu.Lock()
			for name, create := range regexAge {
				if time.Now().Sub(create) > regexAgeLimit {
					delete(regexCache, name)
					delete(regexAge, name)
				}
			}
			regexMu.Unlock()
		}
	}()
}

type regexResp struct {
	e *regexp.Regexp
	r string
}

func regexSet(name string, exp *regexp.Regexp, resp string) {
	regexMu.Lock()
	defer regexMu.Unlock()
	regexCache[name] = regexResp{
		e: exp,
		r: resp,
	}
	regexAge[name] = time.Now()
}

func regexDel(name string) {
	regexMu.Lock()
	defer regexMu.Unlock()
	delete(regexCache, name)
	delete(regexAge, name)
}

func regexGet(name string) (*regexp.Regexp, string, bool) {
	regexMu.RLock()
	defer regexMu.RUnlock()
	r, ok := regexCache[name]
	return r.e, r.r, ok
}
