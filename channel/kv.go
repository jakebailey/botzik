package channel

import "github.com/xyproto/pinterface"

func (c Channel) kv(name string) (pinterface.IKeyValue, error) {
	return c.db.NewKeyValue(c.id("kv", name))
}

// KV returns the key value store of the specified name.
func (c Channel) KV(name string) (pinterface.IKeyValue, error) {
	return c.kv(externalPrefix + name)
}
