package channel

import (
	"log"
	"sync"
)

const (
	prefixKey     = "prefix"
	prefixDefault = "$"
)

var (
	prefixCache = make(map[string]string)
	prefixMu    = sync.RWMutex{}
)

func prefixCacheGet(c string) (string, bool) {
	prefixMu.RLock()
	defer prefixMu.RUnlock()
	p, ok := prefixCache[c]
	return p, ok
}

func prefixCacheSet(c, p string) {
	prefixMu.Lock()
	defer prefixMu.Unlock()
	prefixCache[c] = p
}

// Prefix returns the channel's prefix, or sets it to the default.
func (c Channel) Prefix() (string, error) {
	if p, ok := prefixCacheGet(c.Name); ok {
		return p, nil
	}
	return c.GlobalKVGetDefault(prefixKey, prefixDefault)
}

// SetPrefix sets a channel's command prefix.
func (c Channel) SetPrefix(p string) error {
	log.Println("setting prefix for", c.Name, "to", p)
	if err := c.GlobalKVSet(prefixKey, p); err != nil {
		return err
	}
	prefixCacheSet(c.Name, p)
	return nil
}
