package channel

import (
	"sync"
	"time"
)

const (
	rateKey     = "rate"
	rateDefault = 2 * time.Second
)

var (
	rateCache = make(map[string]time.Duration)
	rateMu    = sync.RWMutex{}
)

func rateCacheGet(c string) (time.Duration, bool) {
	rateMu.RLock()
	defer rateMu.RUnlock()
	r, ok := rateCache[c]
	return r, ok
}

func rateCacheSet(c string, r time.Duration) {
	rateMu.Lock()
	defer rateMu.Unlock()
	rateCache[c] = r
}

// Rate returns the channel's prefix, or sets it to the default.
func (c Channel) Rate() (time.Duration, error) {
	if r, ok := rateCacheGet(c.Name); ok {
		return r, nil
	}
	rateStr, err := c.GlobalKVGetDefault(rateKey, rateDefault.String())
	if err != nil {
		return time.Duration(0), err
	}
	rate, err := time.ParseDuration(rateStr)
	if err != nil {
		return time.Duration(0), err
	}
	return rate, err
}

// SetRate sets a channel's command rate.
func (c Channel) SetRate(rate time.Duration) error {
	if err := c.GlobalKVSet(rateKey, rate.String()); err != nil {
		return err
	}
	rateCacheSet(c.Name, rate)
	return nil
}
