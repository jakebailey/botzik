package channel

import (
	"strconv"

	"bitbucket.org/jakebailey/botzik/db"
	"github.com/xyproto/pinterface"
)

const (
	arrayLengthKey = "length"
)

// Array wraps a KV as an array.
type Array struct {
	kv pinterface.IKeyValue
}

func (c Channel) array(name string) (Array, error) {
	kv, err := c.db.NewKeyValue(c.id("array", name))
	return Array{kv}, err
}

// Array returns the array of the specified name.
func (c Channel) Array(name string) (Array, error) {
	return c.array(externalPrefix + name)
}

// Length returns the length of the aray.
func (a Array) Length() (int, error) {
	lengthStr, err := a.kv.Get(arrayLengthKey)
	err = db.Error(err)
	if err != nil {
		if err != db.ErrKeyNotFound {
			return -1, err
		}
		if err = a.kv.Set(arrayLengthKey, "0"); err != nil {
			return -1, err
		}
		return 0, nil
	}
	length, err := strconv.Atoi(lengthStr)
	if err != nil {
		return -1, err
	}
	return length, nil
}

// Get gets the element at index `i`.
func (a Array) Get(i int) (string, error) {
	return a.kv.Get(strconv.Itoa(i))
}

// Set sets the element at index `i` to `v`.
func (a Array) Set(i int, v string) error {
	err := a.kv.Set(strconv.Itoa(i), v)
	err = db.Error(err)
	if err != nil {
		return err
	}

	length, err := a.Length()
	err = db.Error(err)
	if err != nil {
		return err
	}

	if i >= length {
		return a.kv.Set(arrayLengthKey, strconv.Itoa(i+1))
	}

	return nil
}

// Append appends `v` to the array and returns the new index.
func (a Array) Append(v string) (int, error) {
	length, err := a.Length()
	err = db.Error(err)
	if err != nil {
		return -1, err
	}
	err = a.Set(length, v)
	err = db.Error(err)
	if err != nil {
		return -1, err
	}
	return length, nil
}

// Delete deletes the item at index `i`.
func (a Array) Delete(i int) error {
	err := a.kv.Del(strconv.Itoa(i))
	return db.Error(err)
}

// Remove removes the list from the database.
func (a Array) Remove() error {
	return a.kv.Remove()
}
