package channel

import (
	"strings"

	"bitbucket.org/jakebailey/botzik/db"
	"github.com/xyproto/pinterface"
)

const (
	regularsKey = "regulars"
)

func (c Channel) regulars() (pinterface.ISet, error) {
	return c.set(regularsKey)
}

// AddRegular adds a regular user.
func (c Channel) AddRegular(name string) error {
	set, err := c.regulars()
	if err != nil {
		return err
	}

	err = db.Error(set.Add(strings.ToLower(name)))
	if err != nil && err != db.ErrExistsInSet {
		return err
	}

	return nil
}

// RemoveRegular removes a regular user.
func (c Channel) RemoveRegular(name string) error {
	set, err := c.regulars()
	if err != nil {
		return err
	}

	return db.Error(set.Del(strings.ToLower(name)))
}

// IsRegular checks if a user is a regular.
func (c Channel) IsRegular(name string) (bool, error) {
	set, err := c.regulars()
	if err != nil {
		return false, err
	}

	has, err := set.Has(strings.ToLower(name))
	err = db.Error(err)
	if err != nil && err != db.ErrFoundIt {
		return false, err
	}
	return has, err
}
