package channel

import (
	"strings"

	"github.com/xyproto/pinterface"
)

const (
	dbPrefix       = "channel"
	externalPrefix = "_"
	globalName     = "global"
)

// Channel represets a channel's data as seen by the database.
type Channel struct {
	Name string
	db   pinterface.ICreator
}

// New creates a new channel, bound to a specific name and db.
func New(c string, db pinterface.ICreator) Channel {
	return Channel{
		Name: c,
		db:   db,
	}
}

func (c Channel) id(ty string, s ...string) string {
	extra := strings.Join(s, ":")
	if len(extra) > 0 {
		extra = ":" + extra
	}
	return dbPrefix + ":" + c.Name + ":" + ty + extra
}
