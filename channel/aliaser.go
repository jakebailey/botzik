package channel

import "github.com/xyproto/pinterface"

// Aliaser manages a channel's command aliases.
type Aliaser pinterface.IKeyValue

// Aliaser gets a channel's aliaser.
func (c Channel) Aliaser() (Aliaser, error) {
	return c.db.NewKeyValue(c.id("aliases"))
}
