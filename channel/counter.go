package channel

import (
	"strconv"

	"github.com/xyproto/pinterface"
)

const (
	counterValue = "_value"
)

// Counter is a database-backed integer counter.
type Counter struct {
	kv pinterface.IKeyValue
}

func (c Channel) counter(name string) (Counter, error) {
	kv, err := c.db.NewKeyValue(c.id("counter", name))
	return Counter{kv}, err
}

// Counter returns the counter of the specified name.
func (c Channel) Counter(name string) (Counter, error) {
	return c.counter(externalPrefix + name)
}

// Get gets the value of the counter.
func (c Counter) Get() (int, error) {
	v, err := c.kv.Get(counterValue)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(v)
}

// Set sets the value of the counter.
func (c Counter) Set(i int) error {
	return c.kv.Set(counterValue, strconv.Itoa(i))
}

// Increment increments the counter by a value.
func (c Counter) Increment(i int) (int, error) {
	v, err := c.Get()
	if err != nil {
		return 0, err
	}
	v += i
	if err := c.Set(v); err != nil {
		return 0, err
	}
	return v, nil
}

// Remove removes the counter from the database.
func (c Counter) Remove() error {
	return c.kv.Remove()
}
