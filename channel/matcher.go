package channel

import (
	"regexp"

	"github.com/xyproto/pinterface"
)

const (
	matcherSetID  = "matcher_set"
	matcherExprID = "matcher_expr"
	matcherRespID = "matcher_resp"
)

// Matcher matches messages with
type Matcher struct {
	channel string
	set     pinterface.ISet
	expr    pinterface.IKeyValue
	resp    pinterface.IKeyValue
}

// Matcher gets this channel's matcher.
func (c Channel) Matcher() (Matcher, error) {
	set, err := c.set(matcherSetID)
	if err != nil {
		return Matcher{}, err
	}
	expr, err := c.kv(matcherExprID)
	if err != nil {
		return Matcher{}, err
	}
	resp, err := c.kv(matcherRespID)
	return Matcher{channel: c.Name, set: set, expr: expr, resp: resp}, err
}

// Set sets a named expression with a given response.
func (m Matcher) Set(name, expression, response string) error {
	regex, err := regexp.Compile(expression)
	if err != nil {
		return err
	}
	if err := m.set.Add(name); err != nil {
		return err
	}
	if err := m.expr.Set(name, expression); err != nil {
		return err
	}
	if err := m.resp.Set(name, response); err != nil {
		return err
	}
	regexSet(m.id(name), regex, response)
	return nil
}

// Get gets the expression and response for the specified name.
func (m Matcher) Get(name string) (e string, r string, err error) {
	e, err = m.expr.Get(name)
	if err != nil {
		return "", "", err
	}
	r, err = m.resp.Get(name)
	if err != nil {
		return "", "", err
	}
	return e, r, nil
}

// Delete removes a named match from the matcher.
func (m Matcher) Delete(name string) error {
	regexDel(m.id(name))
	if err := m.set.Del(name); err != nil {
		return err
	}
	if err := m.expr.Del(name); err != nil {
		return err
	}
	return m.resp.Del(name)
}

// All returns a list of all names.
func (m Matcher) All() ([]string, error) {
	return m.set.GetAll()
}

// Match checks the matcher for a match against the given input.
func (m Matcher) Match(input string) (string, error) {
	all, err := m.set.GetAll()
	if err != nil {
		return "", err
	}

	for _, name := range all {
		id := m.id(name)
		var regex *regexp.Regexp
		var resp string

		if e, r, ok := regexGet(id); ok {
			regex = e
			resp = r
		} else {
			expr, err := m.expr.Get(name)
			if err != nil {
				return "", err
			}
			resp, err = m.resp.Get(name)
			if err != nil {
				return "", err
			}
			regex, err = regexp.Compile(expr)
			if err != nil {
				return "", err
			}
			regexSet(id, regex, resp)
		}

		if regex.MatchString(input) {
			return resp, nil
		}
	}

	return "", nil
}

func (m Matcher) id(name string) string {
	return m.channel + ":" + name
}
