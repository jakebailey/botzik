package channel

import "github.com/xyproto/pinterface"

func (c Channel) list(name string) (pinterface.IList, error) {
	return c.db.NewList(c.id("list", name))
}

// List returns the list of the specified name.
func (c Channel) List(name string) (pinterface.IList, error) {
	return c.list(externalPrefix + name)
}
