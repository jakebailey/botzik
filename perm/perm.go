package perm

// Level represents tiered permission levels. Lower numbers indicate higher
// levels of authority, such that the default value is most restrictive when
// declaring a command.
type Level int

// All valid perission levels
const (
	Unknown Level = iota
	Superuser
	Broadcaster
	Mod
	Sub
	Regular
	Normal
	Nobody
)

// Allowed returns true if a user level is allowed to use a command.
func Allowed(user, command Level) bool {
	if user == Unknown || command == Unknown {
		return false
	}
	return user <= command
}

//go:generate stringer -type=Level
