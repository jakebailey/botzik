package perm

import "testing"

func TestAllowed(t *testing.T) {
	t.Parallel()

	tests := []struct {
		user     Level
		command  Level
		expected bool
	}{
		{
			user:     Superuser,
			command:  Superuser,
			expected: true,
		},
		{
			user:     Superuser,
			command:  Nobody,
			expected: true,
		},
		{
			user:     Nobody,
			command:  Superuser,
			expected: false,
		},
		{
			user:     Sub,
			command:  Mod,
			expected: false,
		},
		{
			user:     Mod,
			command:  Sub,
			expected: true,
		},
	}

	for _, test := range tests {
		a := Allowed(test.user, test.command)
		if a != test.expected {
			t.Errorf("Allowed(%v, %v) = %v, want %v", test.user, test.command, a, test.expected)
		}
	}
}
