package birc

import (
	"log"
	"net"
	"sync"

	"bitbucket.org/jakebailey/botzik/lock"

	"github.com/jakebailey/irc"
	"golang.org/x/net/context"
	"golang.org/x/time/rate"
)

// DefaultMaxJoined is the default maximum number of joined channels.
const DefaultMaxJoined = 20

// Single represents a single, reconnecting connection to an IRC server.
type Single struct {
	addr           string
	nick, pass     string
	conn           *irc.Conn
	joined         map[string]bool
	joinedMu       sync.RWMutex
	msgLimiter     *rate.Limiter
	joinLimiter    *rate.Limiter
	in, out        chan *irc.Message
	disc           chan struct{}
	wg             sync.WaitGroup
	rec            lock.Lock
	stopping       bool
	NoSend, NoRecv bool
	MaxJoined      int
}

var _ Conn = (*Single)(nil)

// NewSingle creates a new single connection.
func NewSingle(addr, nick, pass string) *Single {
	return &Single{
		addr:        addr,
		nick:        nick,
		pass:        pass,
		joined:      make(map[string]bool),
		msgLimiter:  rate.NewLimiter(MessageRate, MessageBurst),
		joinLimiter: rate.NewLimiter(JoinRate, JoinBurst),
		out:         make(chan *irc.Message),
		in:          make(chan *irc.Message),
		rec:         lock.NewLock(),
	}
}

// Connect connects to the IRC server.
func (s *Single) Connect() error {
	return s.connect()
}

// Disconnect disconnects from the IRC server.
func (s *Single) Disconnect() {
	log.Printf("connection %p disconnecting", s)
	s.stopping = true
	if err := s.conn.Close(); err != nil {
		log.Println(err)
	}
	close(s.out)
	close(s.in)
	s.wg.Wait()
}

// Joined returns the list of joined channels.
func (s *Single) Joined() []string {
	s.joinedMu.RLock()
	defer s.joinedMu.RUnlock()
	var l []string
	for c := range s.joined {
		l = append(l, c)
	}
	return l
}

// NumJoined returns the number of currently joined channels.
func (s *Single) NumJoined() int {
	s.joinedMu.RLock()
	defer s.joinedMu.RUnlock()
	return len(s.joined)
}

// IsJoined returns true if the specified channels is already joined.
func (s *Single) IsJoined(channel string) bool {
	s.joinedMu.RLock()
	defer s.joinedMu.RUnlock()
	return s.joined[channel]
}

// Join attempts to join a channel, and returns true if successful.
func (s *Single) Join(channel string) bool {
	if s.IsJoined(channel) {
		return true
	}

	maxJoined := s.MaxJoined
	if maxJoined == 0 {
		maxJoined = DefaultMaxJoined
	}
	s.joinedMu.RLock()
	full := len(s.joined) >= maxJoined
	s.joinedMu.RUnlock()

	if full {
		return false
	}

	if err := s.joinLimiter.Wait(context.Background()); err != nil {
		log.Println(err)
		return false
	}

	s.joinedMu.Lock()
	s.out <- &irc.Message{
		Command: irc.JOIN,
		Params:  []string{channel},
	}
	s.joined[channel] = true
	s.joinedMu.Unlock()
	return true
}

// Part leaves a channel.
func (s *Single) Part(channel string) {
	s.joinedMu.Lock()
	defer s.joinedMu.Unlock()

	if !s.joined[channel] {
		return
	}
	s.out <- &irc.Message{
		Command: irc.PART,
		Params:  []string{channel},
	}
	delete(s.joined, channel)
}

// Send attempts to send a message, and returns true if successful.
func (s *Single) Send(m *irc.Message) bool {
	if s.NoSend {
		return false
	}

	if s.rec.IsLocked() {
		return false
	}

	if m.Command == irc.PRIVMSG && !s.msgLimiter.Allow() {
		return false
	}

	if m.Command == irc.PRIVMSG {
		if err := s.msgLimiter.Wait(context.Background()); err != nil {
			log.Println(err)
			return false
		}
	}

	s.out <- m
	return true
}

// Incoming returns a channel of incoming messages.
func (s *Single) Incoming() <-chan *irc.Message {
	return s.in
}

// Forward forwards all incoming messages to another channel. If Forward is
// used, do not use the channel Incoming returns.
func (s *Single) Forward(c chan<- *irc.Message) {
	s.wg.Add(1)
	go func() {
		for m := range s.in {
			c <- m
		}
		s.wg.Done()
	}()
}

func (s *Single) connect() error {
	var err error
	if s.conn, err = irc.Dial(s.addr); err != nil {
		return err
	}

	s.disc = make(chan struct{})

	go s.sender()
	s.login()

	go s.receiver()
	go s.reconnector()

	return nil
}

func (s *Single) sender() {
	s.wg.Add(1)
	defer s.wg.Done()

	for {
		select {
		case <-s.disc:
			return
		case m, ok := <-s.out:
			if !ok {
				return
			}

			err := s.conn.Encode(m)
			if err != nil {
				if !closedError(err) {
					log.Println(err)
				}
				return
			}
		}
	}
}

func (s *Single) receiver() {
	s.wg.Add(1)
	defer s.wg.Done()

	for {
		m, err := s.conn.Decode()
		if err != nil {
			if !closedError(err) {
				log.Println(err)
			}
			break
		}

		if m.Command == irc.PING {
			mCp := *m
			mCp.Command = irc.PONG
			s.out <- &mCp
		}

		if s.NoRecv {
			continue
		}

		s.in <- m
	}
	close(s.disc)
}

func (s *Single) reconnector() {
	s.wg.Add(1)
	defer s.wg.Done()

	<-s.disc
	if s.stopping {
		return
	}

	s.rec.Lock()

	log.Printf("connection %p attempting to reconnect", s)
	for i := 0; i < ReconnectAttempts; i++ {
		if err := s.connect(); err != nil {
			log.Println(err)
			continue
		}
		break
	}

	for c := range s.joined {
		s.out <- &irc.Message{
			Command: irc.JOIN,
			Params:  []string{c},
		}
	}

	s.rec.Unlock()
}

func (s *Single) login() {
	s.out <- &irc.Message{
		Command: irc.PASS,
		Params:  []string{s.pass},
	}
	s.out <- &irc.Message{
		Command: irc.NICK,
		Params:  []string{s.nick},
	}
	s.out <- &irc.Message{
		Command:  irc.CAP,
		Params:   []string{irc.CAP_REQ},
		Trailing: "twitch.tv/tags",
	}
}

func closedError(err error) bool {
	if err != nil {
		netOpError, ok := err.(*net.OpError)
		if ok && netOpError.Err.Error() == "use of closed network connection" {
			return true
		}
	}
	return false
}
