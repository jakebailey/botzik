package birc

import (
	"log"
	"sync"
	"time"

	"github.com/jakebailey/irc"
)

// DownsizeInterval is how often a pool will attempt to downsize.
const DownsizeInterval = time.Minute

// SendPool is a pool of IRC connections that are send only. This pool scales
// to prevent rate limiting, and shrinks if needed.
type SendPool struct {
	addr        string
	nick, pass  string
	conns       map[*Single]struct{}
	connsList   []*Single
	mu          sync.RWMutex
	numSkipped  int
	ticker      *time.Ticker
	tickerClose chan struct{}
	wg          sync.WaitGroup
}

// NewSendPool creates a new SendPool with the specified IRC information.
func NewSendPool(addr, nick, pass string) *SendPool {
	return &SendPool{
		addr:        addr,
		nick:        nick,
		pass:        pass,
		conns:       make(map[*Single]struct{}),
		tickerClose: make(chan struct{}),
	}
}

// Connect connects the pool to the IRC server.
func (p *SendPool) Connect() error {
	if _, err := p.add(); err != nil {
		return err
	}
	p.ticker = time.NewTicker(DownsizeInterval)
	go p.downsizer()
	return nil
}

// Disconnect disconnects from the IRC server, waiting until all child
// connections are disconnected.
func (p *SendPool) Disconnect() {
	p.ticker.Stop()
	close(p.tickerClose)
	for c := range p.conns {
		c.Disconnect()
	}
	p.wg.Wait()
}

// Send sends a message with the pool.
func (p *SendPool) Send(m *irc.Message) {
	if p.trySend(m) {
		return
	}
	conn, err := p.add()
	if err != nil {
		log.Println(err)
	}
	conn.out <- m
}

func (p *SendPool) trySend(m *irc.Message) bool {
	p.mu.RLock()
	defer p.mu.RUnlock()

	var i int
	defer func() { p.numSkipped = i }()

	for c := range p.conns {
		if c.Send(m) {
			return true
		}
		i++
	}
	return false
}

func (p *SendPool) add() (*Single, error) {
	p.mu.Lock()
	defer p.mu.Unlock()

	conn := NewSingle(p.addr, p.nick, p.pass)
	conn.NoRecv = true
	if err := conn.Connect(); err != nil {
		return nil, err
	}
	log.Printf("spawning new connection %p", conn)
	p.conns[conn] = struct{}{}
	p.connsList = append(p.connsList, conn)
	return conn, nil
}

func (p *SendPool) downsizer() {
	p.wg.Add(1)
	defer p.wg.Done()

	for {
		select {
		case <-p.ticker.C:
			p.downsize()
		case <-p.tickerClose:
			return
		}
	}
}

func (p *SendPool) downsize() {
	p.mu.Lock()
	defer p.mu.Unlock()

	remove := len(p.conns) - p.numSkipped - 1
	if remove <= 0 {
		return
	}
	log.Printf("disconnecting %v unneeded connections", remove)

	for _, c := range p.connsList[:remove] {
		c.Disconnect()
		delete(p.conns, c)
	}
	p.connsList = p.connsList[remove:]
}
