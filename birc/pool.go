package birc

import (
	"log"
	"sync"

	"github.com/jakebailey/irc"
)

// Pool represents a pool of connections that only grow.
type Pool struct {
	addr       string
	nick, pass string
	sendPool   *SendPool
	conns      map[*Single]bool
	connsMu    sync.RWMutex
	in, out    chan *irc.Message
	wg         sync.WaitGroup
}

var _ Conn = (*Pool)(nil)

// NewPool creates a new pool with the specified information.
func NewPool(addr, nick, pass string) *Pool {
	return &Pool{
		addr:     addr,
		nick:     nick,
		pass:     pass,
		sendPool: NewSendPool(addr, nick, pass),
		conns:    make(map[*Single]bool),
		in:       make(chan *irc.Message),
		out:      make(chan *irc.Message),
	}
}

// Connect connects the pool to the IRC server.
func (p *Pool) Connect() error {
	if _, err := p.add(); err != nil {
		return err
	}
	return p.sendPool.Connect()
}

// Disconnect disconnects the pool from the IRC server, blocking until the child
// connections have disconnected.
func (p *Pool) Disconnect() {
	p.connsMu.Lock()
	defer p.connsMu.Unlock()

	for c := range p.conns {
		c.Disconnect()
	}
	p.sendPool.Disconnect()

	close(p.out)
	close(p.in)

	p.wg.Wait()
}

// Joined returns a list of channels this pool has joined.
func (p *Pool) Joined() []string {
	p.connsMu.RLock()
	defer p.connsMu.RUnlock()

	var l []string
	for c := range p.conns {
		l = append(l, c.Joined()...)
	}
	return l
}

// NumJoined returns the number of currenly joined channels.
func (p *Pool) NumJoined() int {
	p.connsMu.RLock()
	defer p.connsMu.RUnlock()

	var count int
	for c := range p.conns {
		count += c.NumJoined()
	}
	return count
}

// IsJoined returns true if the specified channel is already joined.
func (p *Pool) IsJoined(channel string) bool {
	p.connsMu.RLock()
	defer p.connsMu.RUnlock()

	for c := range p.conns {
		if c.IsJoined(channel) {
			return true
		}
	}
	return false
}

// Join joins a channel.
func (p *Pool) Join(channel string) bool {
	if p.tryJoin(channel) {
		return true
	}

	conn, err := p.add()
	if err != nil {
		log.Println(err)
		return false
	}
	if conn.Join(channel) {
		return true
	}

	log.Printf("could not join %v, even with a new connection", channel)
	return false
}

func (p *Pool) tryJoin(channel string) bool {
	p.connsMu.RLock()
	defer p.connsMu.RUnlock()

	for c := range p.conns {
		if c.Join(channel) {
			return true
		}
	}
	return false
}

// Part leaves a channel.
func (p *Pool) Part(channel string) {
	p.connsMu.RLock()
	defer p.connsMu.RUnlock()

	for c := range p.conns {
		if c.IsJoined(channel) {
			c.Part(channel)
			return
		}
	}

	log.Printf("unable to part %v", channel)
}

// Send attempts to send a message, and returns true if successful.
func (p *Pool) Send(m *irc.Message) bool {
	p.sendPool.Send(m)
	return true
}

// Incoming returns a channel of incoming messages.
func (p *Pool) Incoming() <-chan *irc.Message {
	return p.in
}

// Forward forwards all incoming messages to another channel. If Forward is
// used, do not use the channel Incoming returns.
func (p *Pool) Forward(c chan<- *irc.Message) {
	p.wg.Add(1)
	go func() {
		for m := range p.in {
			c <- m
		}
		p.wg.Done()
	}()
}

func (p *Pool) add() (*Single, error) {
	p.connsMu.Lock()
	defer p.connsMu.Unlock()

	conn := NewSingle(p.addr, p.nick, p.pass)
	if err := conn.Connect(); err != nil {
		return nil, err
	}
	log.Printf("spawning new connection %p", conn)

	p.conns[conn] = true
	conn.Forward(p.in)

	return conn, nil
}
