package birc

import "golang.org/x/time/rate"

// Regular message constants.
const (
	MessageInterval            = 30
	MessageCount               = 15
	MessageBurst               = 10
	MessageRate     rate.Limit = MessageCount / MessageInterval
)

// Join message constants.
const (
	JoinInterval            = 15
	JoinCount               = 50
	JoinBurst               = 25
	JoinRate     rate.Limit = JoinCount / JoinInterval
)

// ReconnectAttempts is the number of reconnects that a single connection should
// attempt to make.
const ReconnectAttempts = 3
