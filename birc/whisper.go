package birc

import (
	"errors"

	"github.com/jakebailey/irc"
)

// Whisper manages the connection to the Twitch whisper service. This connection
// cannot join other channels.
type Whisper struct {
	*Single
}

// NewWhisper creates a new whisper connection.
func NewWhisper(addr, nick, pass string) Whisper {
	return Whisper{NewSingle(addr, nick, pass)}
}

// Connect connects to the IRC server, and sends the needed control messages to
// recieve whispers.
func (w Whisper) Connect() error {
	if err := w.Single.Connect(); err != nil {
		return err
	}
	w.Single.out <- &irc.Message{
		Command:  irc.CAP,
		Params:   []string{irc.CAP_REQ},
		Trailing: "twitch.tv/commands",
	}
	if !w.Single.Join("#jtv") {
		return errors.New("couldn't join #jtv, this should never happen")
	}
	return nil
}

// Join returns false.
func (w Whisper) Join(channel string) bool {
	return false
}
