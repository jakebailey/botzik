package birc

import "github.com/jakebailey/irc"

// IsPrivMSG returns true if the given message is a valid PRIVMSG, that is, is
// a PRIVMSG, has a preifx, and has params.
func IsPrivMSG(m *irc.Message) bool {
	return m.Command == irc.PRIVMSG && m.Prefix != nil && len(m.Params) != 0
}
