package birc

import "github.com/jakebailey/irc"

// Conn describes an IRC connection.
type Conn interface {
	Connect() error
	Disconnect()
	Joined() []string
	NumJoined() int
	IsJoined(channel string) bool
	Join(channel string) bool
	Part(channel string)
	Send(m *irc.Message) bool
	Incoming() <-chan *irc.Message
	Forward(chan<- *irc.Message)
}
