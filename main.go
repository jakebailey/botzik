package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"

	"bitbucket.org/jakebailey/botzik/bot"
	_ "bitbucket.org/jakebailey/botzik/log"
	"bitbucket.org/jakebailey/botzik/twitch"
	"github.com/namsral/flag"
)

var (
	addr       = flag.String("addr", "irc.twitch.tv:6667", "irc address")
	nick       = flag.String("nick", "botzik", "irc nick")
	pass       = flag.String("pass", "", "irc pass")
	dbType     = flag.String("db_type", "bolt", "database type, bolt or redis")
	dbOptions  = flag.String("db_options", "bolt.db", "database options, filename for bolt, address for redis")
	superusers = flag.String("superusers", "zikaeroh", "comma separated list of superusers")
	initialPre = flag.String("initial", "zikaeroh", "comma separated list of initial channels, without #, bot will also join its own channel")
)

func main() {
	flag.String("config", "", "config path")
	flag.Parse()
	if *pass == "" {
		log.Fatalln("pass must be provided")
	}

	*nick = strings.ToLower(*nick)
	if !twitch.ValidUsername(*nick) {
		log.Fatalf("invalid nick %v", *nick)
	}

	var initial []string
	for _, c := range strings.Split(*initialPre, ",") {
		if !twitch.ValidUsername(c) {
			log.Fatalf("invalid channel %v", c)
		}
		initial = append(initial, "#"+c)
	}
	initial = append(initial, "#"+*nick)

	supers := strings.Split(*superusers, ",")
	for _, u := range supers {
		if !twitch.ValidUsername(u) {
			log.Fatalf("invalid username %v", u)
		}
	}

	b := bot.New(bot.Options{
		Addr:       *addr,
		Nick:       *nick,
		Pass:       *pass,
		DBType:     *dbType,
		DBOptions:  *dbOptions,
		Superusers: supers,
		Initial:    initial,
	})

	if err := b.Start(); err != nil {
		log.Fatal(err)
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	fmt.Print("\r\r\b")

	go func() {
		b.Stop()
		close(c)
	}()

	if _, ok := <-c; ok {
		fmt.Print("\r\r\b")
	}
}
