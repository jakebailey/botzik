package lock

// Lock is a lock whose status can be checked.
type Lock chan struct{}

// NewLock creates a new Lock.
func NewLock() Lock {
	l := make(chan struct{}, 1)
	go func() { l <- struct{}{} }()
	return Lock(l)
}

// Lock locks the lock.
func (l Lock) Lock() {
	<-l
}

// Unlock unlocks the lock.
func (l Lock) Unlock() {
	l <- struct{}{}
}

// IsLocked checks if the lock is locked.
func (l Lock) IsLocked() bool {
	select {
	case <-l:
		go func() { l <- struct{}{} }()
		return false
	default:
		return true
	}
}
